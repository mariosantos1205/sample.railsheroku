class CreateAlmoxarifados < ActiveRecord::Migration[6.1]
  def change
    create_table :almoxarifados do |t|
      t.string :descricao

      t.timestamps
    end
  end
end
