class AlmoxarifadosController < ApplicationController
  before_action :set_almoxarifado, only: [:show, :edit, :update, :destroy]

  # GET /almoxarifados
  # GET /almoxarifados.json
  def index
    @almoxarifados = Almoxarifado.all
  end

  # GET /almoxarifados/1
  # GET /almoxarifados/1.json
  def show
  end

  # GET /almoxarifados/new
  def new
    @almoxarifado = Almoxarifado.new
  end

  # GET /almoxarifados/1/edit
  def edit
  end

  # POST /almoxarifados
  # POST /almoxarifados.json
  def create
    @almoxarifado = Almoxarifado.new(almoxarifado_params)

    respond_to do |format|
      if @almoxarifado.save
        format.html { redirect_to @almoxarifado, notice: 'Almoxarifado was successfully created.' }
        format.json { render :show, status: :created, location: @almoxarifado }
      else
        format.html { render :new }
        format.json { render json: @almoxarifado.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /almoxarifados/1
  # PATCH/PUT /almoxarifados/1.json
  def update
    respond_to do |format|
      if @almoxarifado.update(almoxarifado_params)
        format.html { redirect_to @almoxarifado, notice: 'Almoxarifado was successfully updated.' }
        format.json { render :show, status: :ok, location: @almoxarifado }
      else
        format.html { render :edit }
        format.json { render json: @almoxarifado.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /almoxarifados/1
  # DELETE /almoxarifados/1.json
  def destroy
    @almoxarifado.destroy
    respond_to do |format|
      format.html { redirect_to almoxarifados_url, notice: 'Almoxarifado was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_almoxarifado
      @almoxarifado = Almoxarifado.find(params[:id])
    end

    # Only allow a list of trusted parameters through.
    def almoxarifado_params
      params.require(:almoxarifado).permit(:descricao)
    end
end
